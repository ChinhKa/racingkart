using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Settings", menuName = "Data/Game Settings")]
public class GameSettings : ScriptableObject
{
    [Header("AUDIO CLIP:")]
    public AudioClip backgroundSound;

    public AudioClip brakeSound;

    public AudioClip imPactSound;

    public AudioClip engineSound;

    public AudioClip explosiveSound;

    public AudioClip shieldSound;

    public AudioClip nitroSound;

    public AudioClip finishSound;

    public AudioClip clickSound;

    [Space]
    [Header("CAR:")]
    public List<CarConfigs> cars = new List<CarConfigs>();

    [Space]
    [Header("OTHER CONFIGS:")]
    public float nitroTime;
    public float shieldTime;
    public float breakingTime;
    public float nitroBoostPercent;
    public float timeToRespawn;
    public float distanceToRespawn;

    [Header("REWARDS:")]
    public RewardsConfigs rewardsConfigs;

    [Header("MAP:")]
    public MapConfigs mapConfigs;

    [Header("LEVELS:")]
    public LevelConfigs levelConfigs;
}
