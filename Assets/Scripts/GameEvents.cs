using UniRx;
using UnityEngine;
using UnityEngine.Events;

public class GameEvents : MonoBehaviour
{
	public static UnityEvent onUseShield = new UnityEvent();

	public static UnityEvent onUseNitroBoost = new UnityEvent();

	public static UnityEvent onUpdateNitroText = new UnityEvent();

	public static UnityEvent onUpdateShieldText = new UnityEvent();

	public static UnityEvent onChangeSoundSettings = new UnityEvent();

	public static UnityEvent onCarBrake_Player = new UnityEvent();

	public static UnityEvent onAccelerationCar_Player = new UnityEvent();

	public static GameEvents instance { get; private set; }

	public BoolReactiveProperty gameStarted { get; set; } = new BoolReactiveProperty(initialValue: false);


	public BoolReactiveProperty finishGame { get; set; } = new BoolReactiveProperty(initialValue: false);


	public BoolReactiveProperty gamePause { get; set; } = new BoolReactiveProperty(initialValue: false);


	public IntReactiveProperty currentRank { get; set; } = new IntReactiveProperty();


	private void Awake()
	{
		instance = this;
	}
}
