using TMPro;
using UnityEngine;

public class TXTNitroItem : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI txtNitroItem;

	private void OnEnable()
	{
		UpdateText();
		GameEvents.onUpdateNitroText.AddListener(UpdateText);
	}

	private void OnDisable()
	{
		GameEvents.onUpdateNitroText.RemoveListener(UpdateText);
	}

	private void UpdateText()
	{
		txtNitroItem.text = Prefs.NITRO_ITEM.ToString();
		if(UIManager.ins != null)
			UIManager.ins.gamePlayWindow.ActiveButtonNitroItem();
	}
}
