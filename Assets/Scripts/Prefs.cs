using UnityEngine;

public class Prefs : MonoBehaviour
{
	public static int LEVEL
	{
		get
		{
			return PlayerPrefs.GetInt(ContantKey.LEVEL);
		}
		set
		{
			PlayerPrefs.SetInt(ContantKey.LEVEL, value);
		}
	}

	public static int SHIELD_ITEM
	{
		get
		{
			return PlayerPrefs.GetInt(ContantKey.SHIELD_ITEM);
		}
		set
		{
			PlayerPrefs.SetInt(ContantKey.SHIELD_ITEM, value);
		}
	}

	public static int NITRO_ITEM
	{
		get
		{
			return PlayerPrefs.GetInt(ContantKey.NITRO_ITEM);
		}
		set
		{
			PlayerPrefs.SetInt(ContantKey.NITRO_ITEM, value);
		}
	}

	public static string CAR_SELECTED_ID
	{
		get
		{
			return PlayerPrefs.GetString(ContantKey.CAR_SELECTED_ID);
		}
		set
		{
			PlayerPrefs.SetString(ContantKey.CAR_SELECTED_ID, value);
		}
	}

	public static int SOUND_STATE
	{
		get
		{
			return PlayerPrefs.GetInt(ContantKey.SOUND_STATE);
		}
		set
		{
			PlayerPrefs.SetInt(ContantKey.SOUND_STATE, value);
		}
	}

	public static int MUSIC_STATE
	{
		get
		{
			return PlayerPrefs.GetInt(ContantKey.MUSIC_STATE);
		}
		set
		{
			PlayerPrefs.SetInt(ContantKey.MUSIC_STATE, value);
		}
	}

	public static int VIBRATE_STATE
	{
		get
		{
			return PlayerPrefs.GetInt(ContantKey.VIBRATE_STATE);
		}
		set
		{
			PlayerPrefs.SetInt(ContantKey.VIBRATE_STATE, value);
		}
	}

	public static int GOT_FIRST_FREE_ITEMS_CORE
	{
		get
		{
			return PlayerPrefs.GetInt(ContantKey.GOT_FIRST_FREE_ITEMS_CORE);
		}
		set
		{
			PlayerPrefs.SetInt(ContantKey.GOT_FIRST_FREE_ITEMS_CORE, value);
		}
	}
}
