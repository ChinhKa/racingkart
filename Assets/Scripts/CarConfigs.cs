using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Car Configs", menuName = "Data/Car/Car Configs")]
public class CarConfigs : ScriptableObject
{
    public string id;
    public string name;
    public Transform prefab;
    public float handling;
    public float speed;
    public float accelerate;
}
