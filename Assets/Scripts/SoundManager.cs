using System.Collections;
using UniRx;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private CompositeDisposable subscriptions = new CompositeDisposable();

    public static SoundManager ins;

    public AudioSource _BGAudioSource;

    public AudioSource _VFXAudioSource;

    private void OnEnable()
    {        
        StartCoroutine(Subscribe());

        GameEvents.onChangeSoundSettings.AddListener(CheckSoundSettings);

        CheckSoundSettings();
    }

    private void OnDisable()
    {
        subscriptions.Clear();
        GameEvents.onChangeSoundSettings.RemoveListener(CheckSoundSettings);
    }

    private IEnumerator Subscribe()
    {
        yield return new WaitUntil(() => GameEvents.instance != null);
        GameEvents.instance.finishGame.Subscribe(delegate (bool x)
        {
            if (x)
            {
                PlayEffectSound(DataManager.ins.gameSettings.finishSound);
            }
        }).AddTo(subscriptions);
    }

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        PlayBGMusic(DataManager.ins.gameSettings.backgroundSound, true);
    }

    public void PlayBGMusic(AudioClip clip, bool isLoop)
    {
        if (Prefs.MUSIC_STATE == 0)
        {
            _BGAudioSource.clip = clip;
            _BGAudioSource.loop = isLoop;
        }
    }

    public void PlayEffectSound(AudioClip clip)
    {
        if (Prefs.SOUND_STATE == 0 && clip != null)
        {
            _VFXAudioSource.PlayOneShot(clip);
        }
    }

    public void ClickSound() => PlayEffectSound(DataManager.ins.gameSettings.clickSound);

    private void CheckSoundSettings()
    {
        Debug.Log("Sound:" + Prefs.SOUND_STATE);
        Debug.Log("Music" + Prefs.MUSIC_STATE);
        Debug.Log("Vibrate" + Prefs.VIBRATE_STATE);
        _BGAudioSource.mute = Prefs.MUSIC_STATE == 1 ? true : false;
        _VFXAudioSource.mute = Prefs.SOUND_STATE == 1 ? true : false;
    }
}
