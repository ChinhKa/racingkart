using TMPro;
using UnityEngine;

public class TXTLevelShow : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI txtLevel;

	private void OnEnable()
	{
		UpdateText();
	}

	private void UpdateText()
	{
		txtLevel.text = "LEVEL " + Prefs.LEVEL;
	}
}
