using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SettingWindow : Window
{
    public RectTransform content;

    [SerializeField] private Button btnCloseWindow;

    [Header("SOUND:")]
    [SerializeField] private Button btnTurnOn_Sound;
    [SerializeField] private Button btnTurnOff_Sound;

    [Header("MUSIC:")]
    [SerializeField] private Button btnTurnOn_Music;
    [SerializeField] private Button btnTurnOff_Music;

    [Header("VIBRATE:")]
    [SerializeField] private Button btnTurnOn_Vibrate;
    [SerializeField] private Button btnTurnOff_Vibrate;

    private void OnEnable()
    {
        Effect();
    }

    private void Start()
    {
        ClickEvent();
        ActiveButton(btnTurnOn_Music, btnTurnOff_Music, Prefs.MUSIC_STATE);
        ActiveButton(btnTurnOn_Sound, btnTurnOff_Sound, Prefs.SOUND_STATE);
        ActiveButton(btnTurnOn_Vibrate, btnTurnOff_Vibrate, Prefs.VIBRATE_STATE);
    }

    private void Effect()
    {
        content.position = new Vector3(content.position.x, -100, content.position.z);
        content.DOAnchorPosY(0, 0.5f);
    }

    private void ClickEvent()
    {
        btnCloseWindow.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            content.DOAnchorPosY(-2000, 0.5f).OnComplete(() =>
            {
                UIManager.ins.ShowWindow(UIManager.ins.homeWindow);
            });
        });

        btnTurnOn_Music.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            Prefs.MUSIC_STATE = 1;
            ActiveButton(btnTurnOn_Music, btnTurnOff_Music, Prefs.MUSIC_STATE);
        });

        btnTurnOff_Music.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            Prefs.MUSIC_STATE = 0;
            ActiveButton(btnTurnOn_Music, btnTurnOff_Music, Prefs.MUSIC_STATE);
        });

        btnTurnOn_Sound.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            Prefs.SOUND_STATE = 1;
            ActiveButton(btnTurnOn_Sound, btnTurnOff_Sound, Prefs.SOUND_STATE);
        });

        btnTurnOff_Sound.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            Prefs.SOUND_STATE = 0;
            ActiveButton(btnTurnOn_Sound, btnTurnOff_Sound, Prefs.SOUND_STATE);
        });

        btnTurnOn_Vibrate.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            Prefs.VIBRATE_STATE = 1;
            ActiveButton(btnTurnOn_Vibrate, btnTurnOff_Vibrate, Prefs.VIBRATE_STATE);
        });

        btnTurnOff_Vibrate.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            Prefs.VIBRATE_STATE = 0;
            ActiveButton(btnTurnOn_Vibrate, btnTurnOff_Vibrate, Prefs.VIBRATE_STATE);
        });
    }

    private void ActiveButton(Button btnOn, Button btnOff, int state)
    {
        GameEvents.onChangeSoundSettings?.Invoke();
        if (state == 0)
        {
            btnOn.gameObject.SetActive(true);
            btnOff.gameObject.SetActive(false);
        }
        else
        {
            btnOn.gameObject.SetActive(false);
            btnOff.gameObject.SetActive(true);
        }
    }
}
