using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Level Configs", menuName = "Data/Level/Level Configs")]
public class LevelConfigs : ScriptableObject
{
    public List<Level> levels = new List<Level>();
}

[System.Serializable]
public class Level
{
    public string mapID;
    public List<Bot> bots = new List<Bot>();
}

[System.Serializable]
public class Bot
{
    public string carID;
    public float startDistance;
}