using System.Collections.Generic;
using PathCreation.Examples;
using UnityEngine;

public class RankManager : MonoBehaviour
{
	public static RankManager ins;

	public List<PathFollower> pathFollowers = new List<PathFollower>();

	private void Awake()
	{
		ins = this;
	}

	private void Update()
	{
		SortRank();
	}

	public void SortRank()
	{
		pathFollowers.Sort((PathFollower follower1, PathFollower follower2) => follower2.distanceTravelled.CompareTo(follower1.distanceTravelled));
	}

	public int FindRank(PathFollower path)
	{
		return pathFollowers.FindIndex((PathFollower x) => x.GetInstanceID() == path.GetInstanceID()) + 1;
	}
}
