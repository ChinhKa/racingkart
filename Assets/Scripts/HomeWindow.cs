using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class HomeWindow : Window
{
    public Transform holdToRideLabel;

    public Button btnIncreaseNitroBoost;

    public Button btnIncreaseShield;

    public Button btnCarSelect;

    public Button btnSetting;

    public Transform hand;

    private Tween tw;

    private void OnEnable()
    {
        GameManager.ins.TurnOnCamera(true);
        tw = hand.DOMoveX(transform.position.x + 200, 1).SetLoops(-1, LoopType.Yoyo);
    }

    private void OnDisable()
    {
        DOTween.Kill(tw);
    }

    private void Start()
    {
        ClickEvent();
        Effect();         
    }

    private void Effect()
    {
        btnIncreaseNitroBoost.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
        btnIncreaseShield.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
        holdToRideLabel.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
    }

    private void ClickEvent()
    {
        btnIncreaseNitroBoost.onClick.AddListener(delegate
        {
            SoundManager.ins.ClickSound();
            GameManager.ins.IncreaseNitroBoost();
        });

        btnIncreaseShield.onClick.AddListener(delegate
        {
            SoundManager.ins.ClickSound();
            GameManager.ins.IncreaseShieldItem();
        });

        btnCarSelect.onClick.AddListener(delegate
        {
            SoundManager.ins.ClickSound();
            GameManager.ins.TurnOnCamera(false);
            UIManager.ins.ShowWindow(UIManager.ins.carSelectWindow);
        });

        btnSetting.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            UIManager.ins.ShowWindow(UIManager.ins.settingWindow);
        });
    }

    public void StartGame()
    {
        UIManager.ins.ShowWindow(UIManager.ins.gamePlayWindow);
        UIManager.ins.ShowPopup(UIManager.ins.coreItemsPopup);
        GameManager.ins.TurnOnCamera(true, true);
    }
}
