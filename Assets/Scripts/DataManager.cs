using UnityEngine;

public class DataManager : MonoBehaviour
{
	public static DataManager ins;

	public GameSettings gameSettings;

	private void Awake()
	{
		ins = this;
	}

	private void Start()
	{
		if (!PlayerPrefs.HasKey(ContantKey.LEVEL))
		{
			Prefs.LEVEL = 1;
		}
		if (!PlayerPrefs.HasKey(ContantKey.SHIELD_ITEM))
		{
			Prefs.SHIELD_ITEM = 1;
		}
		if (!PlayerPrefs.HasKey(ContantKey.NITRO_ITEM))
		{
			Prefs.NITRO_ITEM = 1;
		}
		if (!PlayerPrefs.HasKey(ContantKey.CAR_SELECTED_ID))
		{
			Prefs.CAR_SELECTED_ID = "0";
		}
		if (!PlayerPrefs.HasKey(ContantKey.SOUND_STATE))
		{
			Prefs.SOUND_STATE = 0;
		}
		if (!PlayerPrefs.HasKey(ContantKey.MUSIC_STATE))
		{
			Prefs.MUSIC_STATE = 0;
		}
		if (!PlayerPrefs.HasKey(ContantKey.VIBRATE_STATE))
		{
			Prefs.VIBRATE_STATE = 0;
		}
		if (!PlayerPrefs.HasKey(ContantKey.GOT_FIRST_FREE_ITEMS_CORE))
		{
			Prefs.GOT_FIRST_FREE_ITEMS_CORE = 0;
		}
	}
}
