using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class CoreItemsPopup : Popup
{
    public TextMeshProUGUI txtCoreName;
    public TextMeshProUGUI txtButtonName;

    public Image iconAds;
    public Button btnClaim;
    public Button btnClose;
    public Transform content;
    public RectTransform groupButton;
    public GameObject carIns;
    public ParticleSystem nitroEffect;
    public ParticleSystem ShieldEffect;
    bool isNitro;

    private void OnEnable()
    {
        content.localScale = Vector3.zero;
        content.DOScale(Vector3.one, 0.5f);
        carIns.SetActive(true);

        groupButton.localPosition = new Vector3(content.localPosition.x, -1000, content.localPosition.z);
        groupButton.DOAnchorPosY(0, 0.5f);

        isNitro = Random.value > 0.5f ? true : false;
        if (isNitro)
        {
            nitroEffect.gameObject.SetActive(true);
            nitroEffect.Play();
            ShieldEffect.gameObject.SetActive(false);
        }
        else
        {
            nitroEffect.gameObject.SetActive(false);
            ShieldEffect.gameObject.SetActive(true);
            ShieldEffect.Play();
        }

        txtCoreName.text = isNitro ? "Nitro" : "Shield";

        if (Prefs.GOT_FIRST_FREE_ITEMS_CORE == 0)
        {
            iconAds.gameObject.SetActive(false);
            txtButtonName.text = "GET FREE";
        }
        else
        {
            iconAds.gameObject.SetActive(true);
            txtButtonName.text = "CLAIM";
        }
    }

    private void Start()
    {
        btnClaim.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            if (isNitro)
            {
                Prefs.NITRO_ITEM++;
                GameEvents.onUpdateNitroText?.Invoke();
            }
            else
            {
                Prefs.SHIELD_ITEM++;
                GameEvents.onUpdateShieldText?.Invoke();
            }

            Hide();
        });

        btnClose.onClick.AddListener(() =>
        {
            SoundManager.ins.ClickSound();
            Hide();
        });
    }

    private void Hide()
    {
        Prefs.GOT_FIRST_FREE_ITEMS_CORE++;
        groupButton.DOAnchorPosY(-1000, 0.4f).OnComplete(() =>
        {
            carIns.SetActive(false);
            UIManager.ins.ShowPopup();
            if (!GameEvents.instance.gameStarted.Value)
            {
                GameManager.ins.TurnOnCamera(true, false);
                GameManager.ins.StartGame();
            }
        });
    }
}
