﻿using System.Collections;
using PathCreation.Examples;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

public class Movement : MonoBehaviour
{
    private CompositeDisposable subscriptions = new CompositeDisposable();
    public bool _AI;
    public Transform directPoint_AI;

    [Space]
    [Header("PATH:")]
    [SerializeField]
    private PathFollower pathFollower;

    [SerializeField]
    private float limitX;

    private float sidewaySpeed;

    [Space]
    [Header("ELEMENT:")]
    [SerializeField]
    private Rigidbody rigidbody;

    [SerializeField]
    private Transform playerModel;

    private float _finalPos;

    private float _currentPos;

    private float percentageX;

    [Space]
    [Header("RAYCAST:")]
    private RaycastHit hitForward;

    private RaycastHit hitLeft;

    private RaycastHit hitRight;

    [Space]
    [Header("WHEEL:")]
    [SerializeField]
    private Transform comboWheelFront;

    [SerializeField]
    private Transform comboWheelBack;

    public AudioSource audioSource_Skid;
    public AudioSource audioSource_Collision;
    public AudioSource audioSource_Engine;

    public ParticleSystem nitroEffect;

    public ParticleSystem shieldEffect;

    public ParticleSystem explosive;

    private bool usingShield;

    private bool usingNitroBoost;

    private bool isDead;

    private bool isShowCar;

    private bool isBreaking;
    private float coolDownBrake;

    private float timeRotate;
    float clampedY;
    private int signal = 1;

    private Transform parent;
    public ParticleSystem driftEffect;
    public ParticleSystem smokeEffect_Contact;
    public ParticleSystem respawnEffect;
    private float touchTime;
    public GroundCheckPoint groundCheckPoint;
    private bool disableCheckGround;

    private void OnEnable()
    {
        StartCoroutine(Subscribe());

        GameEvents.onChangeSoundSettings.AddListener(CheckSoundSettings);

        GameEvents.onUseShield.AddListener(delegate
        {
            if (!usingShield && !_AI)
            {
                StartCoroutine(ShieldHandle());
            }
        });
        GameEvents.onUseNitroBoost.AddListener(delegate
        {
            if (!usingNitroBoost && !_AI)
            {
                StartCoroutine(NitroBoostHandle());
            }
        });
    }

    private void OnDisable()
    {
        subscriptions.Clear();
        if (!_AI)
        {
            GameEvents.onUseShield.RemoveListener(delegate
            {
                ShowEffect(shieldEffect);
            });
            GameEvents.onUseNitroBoost.RemoveListener(delegate
            {
                ShowEffect(nitroEffect);
            });
        }
    }

    private void Start()
    {
        if (!_AI && pathFollower != null && pathFollower.pathCreator != null)
        {
            UIManager.ins.gamePlayWindow.UpdateSpeedSlider(pathFollower.speed, pathFollower.speedBase);
            UIManager.ins.gamePlayWindow.SetMaxRoadProgressSlider(pathFollower.pathCreator.path.length);
        }
        else
        {
            int value = Random.value <= 0.5f ? 1 : -1;
            playerModel.localPosition += new Vector3(value, 0, 0);
        }
        HideEffect(driftEffect);
        HideEffect(nitroEffect);
        HideEffect(shieldEffect);
        StartCoroutine(DelayPhysic());
    }

    private void Update()
    {
        timeRotate -= Time.deltaTime;
        if (!_AI)
        {
            SignalProcessing();
            Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView - 1 * Time.deltaTime * 5 * signal, 23, 30);
        }
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
        playerModel.localRotation = Quaternion.Lerp(playerModel.localRotation, Quaternion.identity, Time.deltaTime * 2);
        AddforceDown();
        WheelRotate();

        if (groundCheckPoint.isGrounded && _AI || disableCheckGround && _AI || !_AI)
        {
            if (!GameEvents.instance.finishGame.Value && GameEvents.instance.gameStarted.Value)
            {
                if (coolDownBrake > 0)
                {
                    playerModel.localRotation = Quaternion.identity;
                    pathFollower.isAccelerating = false;
                    coolDownBrake -= Time.deltaTime;
                    isBreaking = true;
                }
                else
                {
                    isBreaking = false;
                }

                if (_AI)
                {
                    AIHandle();
                    Move();
                }
                else
                {
                    UIManager.ins.gamePlayWindow.UpdateRoadProgressSlider(pathFollower.distanceTravelled);
                    UIManager.ins.gamePlayWindow.UpdateSpeedSlider(pathFollower.speed, pathFollower.speedBase);
                    UIManager.ins.gamePlayWindow.UpdateTXTCurrentRank(pathFollower.rank);
                }
                if (isDead)
                {
                    pathFollower.speed = 0f;
                }
            }
        }
        else
        {
            pathFollower.isAccelerating = false;
        }
    }

    private void AddforceDown()
    {
        rigidbody.AddForce(-transform.up * 5000, ForceMode.Impulse);
    }

    private IEnumerator DelayPhysic()
    {
        rigidbody.isKinematic = true;
        yield return new WaitForSeconds(0.5f);
        rigidbody.isKinematic = false;
    }

    private IEnumerator NitroBoostHandle()
    {
        usingNitroBoost = true;
        pathFollower.SpeedExpand();
        SoundManager.ins.PlayEffectSound(DataManager.ins.gameSettings.nitroSound);
        ShowEffect(nitroEffect);
        yield return new WaitForSeconds(DataManager.ins.gameSettings.nitroTime);
        usingNitroBoost = false;
        pathFollower.ResetSpeed();
        HideEffect(nitroEffect);
        UIManager.ins.gamePlayWindow.ActiveButtonNitroItem();
    }

    private IEnumerator ShieldHandle()
    {
        usingShield = true;
        SoundManager.ins.PlayEffectSound(DataManager.ins.gameSettings.shieldSound);
        ShowEffect(shieldEffect);
        yield return new WaitForSeconds(DataManager.ins.gameSettings.shieldTime);
        usingShield = false;
        HideEffect(shieldEffect);
        UIManager.ins.gamePlayWindow.ActiveButtonShieldItem();
    }

    private void ShowEffect(ParticleSystem effect)
    {
        if (effect != null)
        {
            effect.gameObject.SetActive(value: true);
            effect.Play();
        }
    }

    private void HideEffect(ParticleSystem effect)
    {
        if (effect != null)
        {
            effect.gameObject.SetActive(value: false);
            effect.Stop();
        }
    }

    private void PlayEffectSound(AudioSource audioSource, AudioClip clip, bool isPlayNow = false)
    {
        if (Prefs.SOUND_STATE == 0 && !_AI && audioSource != null && clip != null)
        {
            if (isPlayNow)
            {
                audioSource.PlayOneShot(clip);
            }
            else
            {
                if (!audioSource.isPlaying)
                    audioSource.PlayOneShot(clip);
            }
        }
    }

    public void WheelDir()
    {
        if (percentageX > 0f)
        {
            Quaternion b = Quaternion.Euler(new Vector3(0f, 10f, 0f));
            comboWheelFront.localRotation = Quaternion.Lerp(comboWheelFront.localRotation, b, Time.deltaTime * 2f);
        }
        else if (percentageX < 0f)
        {
            Quaternion b2 = Quaternion.Euler(new Vector3(0f, -10f, 0f));
            comboWheelFront.localRotation = Quaternion.Lerp(comboWheelFront.localRotation, b2, Time.deltaTime * 2f);
        }
        else if (percentageX == 0f)
        {
            Quaternion identity = Quaternion.identity;
            comboWheelFront.localRotation = Quaternion.Lerp(comboWheelFront.localRotation, identity, Time.deltaTime * 5f);
        }
    }

    public void WheelRotate()
    {
        comboWheelFront.Rotate(new Vector3(pathFollower.speed, 0f, 0f));
        comboWheelBack.Rotate(new Vector3(pathFollower.speed, 0f, 0f));
    }

    private IEnumerator Subscribe()
    {
        yield return new WaitUntil(() => GameEvents.instance != null);
        (from _ in this.UpdateAsObservable()
         where Input.GetMouseButton(0) && !_AI && GameEvents.instance.gameStarted.Value && !GameEvents.instance.finishGame.Value && !isDead || usingNitroBoost && !isDead
         select _).Subscribe(delegate
         {
             if (!isDead && !isBreaking && groundCheckPoint.isGrounded)
             {
                 touchTime -= Time.deltaTime;
                 PlayEffectSound(audioSource_Engine, DataManager.ins.gameSettings.engineSound);
                 if (usingNitroBoost)
                 {
                     pathFollower.Nitro();
                 }
                 pathFollower.isAccelerating = true;
                 if (touchTime <= 0)
                 {
                     Move();
                 }
             }
         }).AddTo(subscriptions);
        (from _ in this.UpdateAsObservable()
         where Input.GetMouseButtonUp(0) && !_AI
         select _).Subscribe(delegate
         {
             if (groundCheckPoint.isGrounded)
             {
                 touchTime = 0.2f;
                 if(!usingNitroBoost)
                    pathFollower.isAccelerating = false;
                 playerModel.localRotation = Quaternion.identity;
                 percentageX = 0f;
                 audioSource_Engine.Stop();
                 audioSource_Skid.Stop();
             }
         }).AddTo(subscriptions);
        GameEvents.instance.finishGame.Subscribe(delegate (bool x)
        {
            if (x)
            {
                pathFollower.isAccelerating = false;
                percentageX = 0f;
            }
        }).AddTo(subscriptions);
    }

    private void Move()
    {
        if (GameEvents.instance.gameStarted.Value && (Input.GetMouseButton(0) || _AI))
        {
            if (!_AI)
            {
                percentageX = (Input.mousePosition.x - (float)(Screen.width / 2)) / ((float)Screen.width * 0.5f) * 2f;
            }
            percentageX = Mathf.Clamp(percentageX, -1f, 1f);
            float num = percentageX * limitX - playerModel.localPosition.x;
            float x = Mathf.Clamp(playerModel.localPosition.x + num * Time.deltaTime * sidewaySpeed, 0f - limitX, limitX);
            playerModel.localPosition = new Vector3(x, playerModel.localPosition.y, playerModel.localPosition.z);
            float y = num / limitX * 30f;
            clampedY = Mathf.Clamp(y, -40, 40);
            Quaternion b = Quaternion.Euler(0f, clampedY, 0f);
            playerModel.localRotation = Quaternion.Lerp(playerModel.localRotation, b, Time.deltaTime * 2);
        }
    }

    private void TurnOnWheelEffect()
    {
        driftEffect.gameObject.SetActive(true);
    }

    private void TurnOffWheelEffect()
    {
        driftEffect.gameObject.SetActive(false);
    }

    private void SignalProcessing()
    {
        if (GameEvents.instance.gameStarted.Value && !GameEvents.instance.finishGame.Value)
        {
            if (Input.GetMouseButton(0))
                signal = -1;
            else
                signal = 1;
        }
    }

    private void AIHandle()
    {
        if (!(timeRotate <= 0f))
        {
            pathFollower.isAccelerating = true;
            return;
        }
        else
        {
            if (!isBreaking)
                pathFollower.isAccelerating = true;
        }
        Vector3 forward = directPoint_AI.forward;
        Vector3 direction = Quaternion.Euler(0f, -10f, 0f) * directPoint_AI.forward;
        Vector3 direction2 = Quaternion.Euler(0f, 10f, 0f) * directPoint_AI.forward;
        float maxDistance = 30;
        if (Physics.Raycast(directPoint_AI.position, forward, out hitForward, maxDistance)
            || Physics.Raycast(directPoint_AI.position, direction, out hitLeft, maxDistance)
            || Physics.Raycast(directPoint_AI.position, direction2, out hitRight, maxDistance))
        {
            percentageX = ((!(Random.value < 0.5f)) ? 1 : (-1));
            timeRotate = 1f;
            Debug.DrawLine(directPoint_AI.position, hitForward.point, Color.red);
            Debug.DrawLine(directPoint_AI.position, hitLeft.point, Color.green);
            Debug.DrawLine(directPoint_AI.position, hitRight.point, Color.blue);
        }
        else
        {
            percentageX = 0;
            playerModel.localRotation = Quaternion.identity;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            if (!_AI)
            {
                GameEvents.instance.finishGame.Value = true;
            }
            this.enabled = false;
            pathFollower.isAccelerating = false;
        }

        if (other.tag == "RespawnZone" && !isDead)
        {
            Respawn();
        }

        if (other.tag == "OverHeadGate")
        {
            var gate = other.transform.GetComponent<OverHeadGate>();
            disableCheckGround = gate.disableCheckGround;
            if (!_AI)
            {
                if (gate.disableRigidbodyFrozen)
                {
                    rigidbody.constraints = RigidbodyConstraints.None;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "OverHeadGate")
        {
            var gate = other.transform.GetComponent<OverHeadGate>();
            disableCheckGround = gate.disableCheckGround;
            if (!_AI)
            {
                if (!gate.disableRigidbodyFrozen)
                {
                    rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!GameEvents.instance.gameStarted.Value)
            return;

        if (collision.gameObject.CompareTag("Obstacle") && !isDead || collision.gameObject.CompareTag("RespawnZone") && !isDead)
        {
            Respawn();
        }

        if (collision.gameObject.CompareTag("Car") || collision.gameObject.CompareTag("SlowObject"))
        {
            PlayEffectSound(audioSource_Collision, DataManager.ins.gameSettings.explosiveSound, true);
            Vibrate();
            ShowEffect(explosive);
            if(!usingShield && !usingNitroBoost)
                coolDownBrake = DataManager.ins.gameSettings.breakingTime;
            collision.transform.GetComponent<Rigidbody>().AddForce(collision.transform.forward * 20, ForceMode.Impulse);
        }

        if (collision.gameObject.CompareTag("DriftGate"))
        {
            if (pathFollower.speed >= pathFollower.speedBase / 3)
            {
                int dir = collision.gameObject.GetComponent<DriftGate>().DriftDirect == DriftDirect.left ? 1 : -1;
                StartCoroutine(Drift(dir));
            }
        }
    }    

    private void Respawn()
    {
        PlayEffectSound(audioSource_Collision, DataManager.ins.gameSettings.explosiveSound, true);
        ShowEffect(explosive);
        Vibrate();

        if (!usingShield && !usingNitroBoost)
        {
            isDead = true;
            smokeEffect_Contact.Play();
            TurnOffWheelEffect();
            rigidbody.isKinematic = true;
            pathFollower.RespawnPos(delegate (bool x)
            {
                if (x)
                {
                    rigidbody.isKinematic = false;
                    respawnEffect.Play();
                    playerModel.localPosition = Vector3.zero;
                    isDead = false;
                }
            });
        }
    }

    private IEnumerator Drift(int dir)
    {
        StopCoroutine(Drift(dir));
        rigidbody.AddForce(transform.right * 200000 * dir, ForceMode.Impulse);
        PlayEffectSound(audioSource_Skid, DataManager.ins.gameSettings.brakeSound);
        TurnOnWheelEffect();
        yield return new WaitForSeconds(1);
        TurnOffWheelEffect();
        audioSource_Skid.Stop();
    }

    public void SetData(CarConfigs data)
    {
        pathFollower.speedBase = data.speed;
        pathFollower.accelerate = data.accelerate;
        sidewaySpeed = data.handling;
    }

    private void Vibrate()
    {
        if (!_AI)
        {
            if (Prefs.VIBRATE_STATE == 0)
                Handheld.Vibrate();
            Camera.main.DOShakePosition(1, 0.5f);
        }
    }

    private void CheckSoundSettings()
    {
        audioSource_Engine.mute =
            audioSource_Skid.mute =
            audioSource_Collision.mute = Prefs.SOUND_STATE == 1 ? true : false;
    }
}
