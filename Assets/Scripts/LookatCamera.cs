using UnityEngine;

public class LookatCamera : MonoBehaviour
{
	private void Update()
	{
		if(Camera.main != null && Camera.main.gameObject.activeInHierarchy)
			base.transform.LookAt(base.transform.position + Camera.main.transform.forward);
	}
}
