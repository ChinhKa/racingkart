using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using PathCreation.Examples;
using System.Collections;

public class CarSelectWindow : Window
{
    public int carIndex;
    public Transform carPos;
    public Transform carIns;

    [Header("BUTTON:")]
    [SerializeField] private Button btnNextCar;
    [SerializeField] private Button btnPreviousCar;
    [SerializeField] private Button btnSelectCar;
    [SerializeField] private Button btnCloseWindow;

    [Space]
    [Header("TXT:")]
    [SerializeField] private TextMeshProUGUI txtCarName;
    [SerializeField] private TextMeshProUGUI txtHandling;
    [SerializeField] private TextMeshProUGUI txtSpeed;
    [SerializeField] private TextMeshProUGUI txtAccelerate;

    private Action onChangeCarSelectedIndex;

    private void OnEnable()
    {
        StartCoroutine(UpdateStatInfo());        
    }

    private void OnDisable()
    {
        if(carIns != null)
            Destroy(carIns.gameObject);
    }

    private void Start()
    {
        ClickEvent();
        onChangeCarSelectedIndex += OnChangeSelectedIndex;
        if (PlayerPrefs.HasKey(ContantKey.CAR_SELECTED_ID))        
            carIndex = int.Parse(Prefs.CAR_SELECTED_ID);        
        else
            carIndex = 0;
    }

    private void ClickEvent()
    {
        btnCloseWindow.onClick.AddListener(() => {
            UIManager.ins.ShowWindow(UIManager.ins.homeWindow);
             SoundManager.ins.ClickSound();
        });
        btnSelectCar.onClick.AddListener(() =>
        {
            Prefs.CAR_SELECTED_ID = carIndex.ToString();
            UIManager.ins.ShowWindow(UIManager.ins.homeWindow);
            GameManager.ins.SpawnPlayerCar();
            SoundManager.ins.ClickSound();
        });

        btnNextCar.onClick.AddListener(() => {
            SoundManager.ins.ClickSound();
            NextCar();
        });
        btnPreviousCar.onClick.AddListener(() => {
            SoundManager.ins.ClickSound();
            PreviousCar();
        });
    }

    private void NextCar()
    {
        int carNextIndex = carIndex + 1;
        carIndex = carIndex < DataManager.ins.gameSettings.cars.Count - 1 ? carNextIndex : 0;
        onChangeCarSelectedIndex?.Invoke();
    }

    private void PreviousCar()
    {
        int maxCarIndex = DataManager.ins.gameSettings.cars.Count - 1;
        int carPreviousIndex = carIndex - 1;
        carIndex = carIndex > 0 ? carPreviousIndex : maxCarIndex;
        onChangeCarSelectedIndex?.Invoke();
    }

    private void OnChangeSelectedIndex()
    {
        StartCoroutine(UpdateStatInfo());
    }

    private IEnumerator UpdateStatInfo()
    {
        yield return new WaitUntil(() => DataManager.ins != null);
        CarConfigs carConfigs = DataManager.ins.gameSettings.cars.Find(c => c.id == carIndex.ToString());

        if (carConfigs != null)
        {
            txtCarName.text = carConfigs.name;
            txtHandling.text = carConfigs.handling.ToString();
            txtSpeed.text = carConfigs.speed.ToString();
            txtAccelerate.text = carConfigs.accelerate.ToString();

            Transform car = Instantiate(carConfigs.prefab, carPos.position + new Vector3(0, 1, 0), Quaternion.identity);
            car.GetComponent<PathFollower>().enabled = false;
            car.GetChild(0).GetComponent<Movement>().enabled = false;
            car.GetComponent<ShowCar>().enabled = true;

            if (carIns != null)
            {
                Destroy(carIns.gameObject);
            }
            carIns = car;
        }
    }
}
