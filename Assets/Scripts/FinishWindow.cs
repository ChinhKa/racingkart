using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FinishWindow : Window
{
    public Transform finishLabel;
    public Transform messLabel;

    //	public TextMeshProUGUI txtRank;

    public Image rankIcon;
    public Sprite[] rankList;

    public TextMeshProUGUI txtContent;

    public TextMeshProUGUI txtShieldReward;
    public TextMeshProUGUI txtNitroReward;

    public Button btnRetry;

    public Button btnNext;

    private Rewards rankingReward;

    private void OnEnable()
    {
        int value = GameEvents.instance.currentRank.Value;
        switch (value)
        {
            case 1:
                //txtRank.text = value + "st";
                txtContent.text = "Congratulations!\nYour reward:";
                Active(btnNext.gameObject);
                break;
            case 2:
                //txtRank.text = value + "nd";
                txtContent.text = "Well done!\nYour reward:";
                Active(btnNext.gameObject);
                break;
            case 3:
                //txtRank.text = value + "rd";
                txtContent.text = "Nice try!\nYour reward:";
                Active(btnNext.gameObject);
                break;
            default:
                //txtRank.text = value + "th";
                txtContent.text = "Oops,level failed!\nYour Ranking: 4th";
                Active(btnRetry.gameObject);
                break;
        }


        if (value < 4 && value > 0)
        {
            rankIcon.gameObject.SetActive(true);
            rankIcon.sprite = rankList[value - 1];
            rankingReward = DataManager.ins.gameSettings.rewardsConfigs.rankingRewards[Prefs.LEVEL - 1].rewards[value - 1];
            txtNitroReward.text = "+" + rankingReward.nitroRewardNbr;
            txtShieldReward.text = "+" + rankingReward.shieldRewardNbr;
        }
        else
        {
            rankIcon.gameObject.SetActive(false);
            txtNitroReward.text = "";
            txtShieldReward.text = "";
        }
        EffectOnEnable();
    }

    public void Active(GameObject btn)
    {
        btnRetry.gameObject.SetActive(value: false);
        btnNext.gameObject.SetActive(value: false);
        if (btn != null)
        {
            btn.SetActive(value: true);
        }
    }

    private void Start()
    {
        Effect();
        ClickEvent();
    }

    private void EffectOnEnable()
    {
        finishLabel.position = new Vector3(-1500, finishLabel.position.y, finishLabel.position.z);
        finishLabel.DOLocalMoveX(0, 0.5f);

        messLabel.position = new Vector3(1500, messLabel.position.y, messLabel.position.z);
        messLabel.DOLocalMoveX(0, 0.5f);

        rankIcon.transform.localScale = Vector3.zero;
        rankIcon.transform.DOScale(Vector3.one, 0.5f);
    }

    private void Effect()
    {
        btnNext.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
        btnRetry.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
    }

    private void ClickEvent()
    {
        btnRetry.onClick.AddListener(delegate
        {
            SoundManager.ins.ClickSound();
            GameManager.ins.LoadLevel(isNext: false);
        });
        btnNext.onClick.AddListener(delegate
        {
            Prefs.NITRO_ITEM += rankingReward.nitroRewardNbr;
            Prefs.SHIELD_ITEM += rankingReward.shieldRewardNbr;

            SoundManager.ins.ClickSound();
            GameManager.ins.LoadLevel(isNext: true);
        });
    }
}
