using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriftGate : MonoBehaviour
{
    public DriftDirect DriftDirect; 
}


public enum DriftDirect
{
    left,
    right
}