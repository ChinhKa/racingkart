using UnityEngine;

public class RotateCore : MonoBehaviour
{
	public float speed;

	private int dir;

	private void OnEnable()
	{
		dir = ((!(Random.value < 0.5f)) ? 1 : (-1));
	}

	private void Update()
	{
		base.transform.Rotate(Vector3.forward * speed * dir * Time.deltaTime);
	}
}
