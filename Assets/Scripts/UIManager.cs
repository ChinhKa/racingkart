using System.Collections;
using UniRx;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private CompositeDisposable subscriptions = new CompositeDisposable();

    public static UIManager ins;

    [Header("WINDOW:")]
    public FinishWindow finishWindow;

    public SettingWindow settingWindow;

    public GamePlayWindow gamePlayWindow;

    public HomeWindow homeWindow;

    public CarSelectWindow carSelectWindow;

    [Space]
    [Header("POPUP:")]
    public CoreItemsPopup coreItemsPopup;

    private void OnEnable()
    {
        StartCoroutine(Subscribe());
    }

    private void OnDisable()
    {
        subscriptions.Clear();
    }

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        ShowWindow(homeWindow);
        ShowPopup();
    }

    private IEnumerator Subscribe()
    {
        yield return new WaitUntil(() => GameEvents.instance != null);

        GameEvents.instance.finishGame.Subscribe(delegate (bool x)
        {
            if (x)
            {
                ShowWindow(finishWindow,0.5f);
            }
        }).AddTo(subscriptions);
    }

    #region Window
    public void ShowWindow(Window window, float delayTime = 0) => StartCoroutine(ShowWinDowHandle(window, delayTime));

    private IEnumerator ShowWinDowHandle(Window window, float delayTime = 0)
    {
        yield return new WaitForSeconds(delayTime);
        homeWindow.gameObject.SetActive(value: false);
        gamePlayWindow.gameObject.SetActive(value: false);
        finishWindow.gameObject.SetActive(value: false);
        settingWindow.gameObject.SetActive(value: false);
        carSelectWindow.gameObject.SetActive(value: false);
        if (window != null)
        {
            window.gameObject.SetActive(value: true);
        }
    }
    #endregion

    #region Popup
    public void ShowPopup(Popup popup = null, float delayTime = 0) => StartCoroutine(ShowPopupHandle(popup, delayTime));

    private IEnumerator ShowPopupHandle(Popup popup, float delayTime = 0)
    {
        yield return new WaitForSeconds(delayTime);
        coreItemsPopup.gameObject.SetActive(value: false);
        if (popup != null)
        {
            popup.gameObject.SetActive(value: true);
        }
    }
    #endregion
}
