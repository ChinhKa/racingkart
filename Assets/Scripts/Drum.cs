using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drum : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform obj;
    [SerializeField] private Transform[] edges;

    [Header("Speed Values")]
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float delayTime;
    [SerializeField] private float movementSpeed;
    private int currentEdge;

    private void Awake()
    {
        int randEdge = Random.Range(0, edges.Length);
        obj.transform.position = edges[randEdge].position;
        currentEdge = System.Convert.ToInt32(!System.Convert.ToBoolean(randEdge));
        MoveToEdge();
    }
    private void Update()
    {
        obj.Rotate(new Vector3(0, Time.deltaTime * rotationSpeed, 0));
    }

    private void MoveToEdge()
    {
        obj.DOMove(edges[currentEdge].position, 1 / movementSpeed).SetDelay(delayTime).SetEase(Ease.Linear)
        .OnComplete(() =>
        {
            ChangeDestination();
            MoveToEdge();
        });
    }
    private void ChangeDestination()
    {
        bool edgeBool = System.Convert.ToBoolean(currentEdge);
        currentEdge = System.Convert.ToInt32(!edgeBool);
    }
}
