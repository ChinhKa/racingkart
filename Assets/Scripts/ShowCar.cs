using UnityEngine;

public class ShowCar : MonoBehaviour
{
	public float speed;

	public GameObject rankLabel;

	public bool enableVFXGroup;

	public GameObject vfx_Group;

	private int dir;

	private void OnEnable()
	{
		dir = ((!(Random.value < 0.5f)) ? 1 : (-1));
			
		if(!enableVFXGroup)
			vfx_Group.SetActive(false);
	}

    private void Start()
    {
		rankLabel.SetActive(false);
    }

    private void Update()
	{
		base.transform.Rotate(Vector3.up * speed * dir * Time.deltaTime);
	}
}
