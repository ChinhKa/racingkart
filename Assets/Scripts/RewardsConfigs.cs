using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Rewards Configs", menuName = "Data/Rewards/Rewards Configs")]

public class RewardsConfigs : ScriptableObject
{
    public List<RankingRewards> rankingRewards = new List<RankingRewards>();
}

[System.Serializable]
public class RankingRewards
{
    public List<Rewards> rewards = new List<Rewards>();
}

[System.Serializable]
public class Rewards
{
    public int nitroRewardNbr;
    public int shieldRewardNbr;
}