using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GamePlayWindow : Window
{
    [SerializeField]
    private TextMeshProUGUI txtSpeed;
    public TextMeshProUGUI txtRankBoard;
    public TextMeshProUGUI txtRankUnit;

    [SerializeField]
    private GameObject[] speedProgress;

    [SerializeField]
    private Slider roadProgressSlider;

    [SerializeField]
    private GameObject quickSpeedEff;

    [SerializeField]
    private Button btnUseShield;

    [SerializeField]
    private Button btnUseNitroBoost;

    private void Start()
    {
        ClickEvent();
        ActiveButtonNitroItem();
        ActiveButtonShieldItem();
        quickSpeedEff.SetActive(value: false);
        Effect();
    }

    private void ClickEvent()
    {
        btnUseShield.onClick.AddListener(Shield);
        btnUseNitroBoost.onClick.AddListener(Nitro);
    }

    private void Effect()
    {
        btnUseNitroBoost.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
        btnUseShield.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f).SetLoops(-1, LoopType.Yoyo);
    }

    public void SetMaxRoadProgressSlider(float value)
    {
        roadProgressSlider.maxValue = value;
        roadProgressSlider.value = 0f;
    }

    public void UpdateTXTCurrentRank(int value)
    {
        txtRankBoard.text = value.ToString();
        int currentRank = GameEvents.instance.currentRank.Value;
        switch (currentRank)
        {
            case 1:
                txtRankUnit.text = "st";
                break;
            case 2:
                txtRankUnit.text = "nd";
                break;
            case 3:
                txtRankUnit.text = "rd";
                break;
            default:
                txtRankUnit.text = "th";
                break;
        }
        GameEvents.instance.currentRank.Value = value;
    }

    public void UpdateRoadProgressSlider(float value)
    {
        roadProgressSlider.value = value;
    }

    public void UpdateSpeedSlider(float speedCurrent, float maxSpeed)
    {
        txtSpeed.text = Mathf.RoundToInt(speedCurrent).ToString();

        foreach (var o in speedProgress)
        {
            o.SetActive(false);
        }

        int amount = (int)((speedCurrent / maxSpeed) * speedProgress.Length);

        for (int i = 0; i < amount; i++)
        {
            if (i < speedProgress.Length)
            {
                speedProgress[i].SetActive(true);
            }
        }

        if (amount >= speedProgress.Length - 1)
        {
            quickSpeedEff.SetActive(value: true);
        }
        else
        {
            quickSpeedEff.SetActive(value: false);
        }
    }

    private void Shield()
    {
        Prefs.SHIELD_ITEM--;
        GameEvents.onUpdateShieldText?.Invoke();
        GameEvents.onUseShield?.Invoke();
        btnUseShield.interactable = false;
    }

    private void Nitro()
    {
        Prefs.NITRO_ITEM--;
        GameEvents.onUpdateNitroText?.Invoke();
        GameEvents.onUseNitroBoost?.Invoke();
        btnUseNitroBoost.interactable = false;
    }

    public void ActiveButtonNitroItem()
    {
        btnUseNitroBoost.interactable = Prefs.NITRO_ITEM > 0 ? true : false;
    }

    public void ActiveButtonShieldItem()
    {
        btnUseShield.interactable = Prefs.SHIELD_ITEM > 0 ? true : false;
    }
}
