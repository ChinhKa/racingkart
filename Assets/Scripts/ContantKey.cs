public class ContantKey
{
	public const string LEVEL = "LEVEL";

	public const string SHIELD_ITEM = "SHIELD_ITEM";

	public const string NITRO_ITEM = "NITRO_ITEM";

	public const string CAR_SELECTED_ID = "CAR_SELECTED_ID";

	public const string SOUND_STATE = "SOUND_STATE";

	public const string MUSIC_STATE = "MUSIC_STATE";

	public const string VIBRATE_STATE = "VIBRATE_STATE";

	public const string GOT_FIRST_FREE_ITEMS_CORE = "GOT_FIRST_FREE_ITEMS_CORE";
}
