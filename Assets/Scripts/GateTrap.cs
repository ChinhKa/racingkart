using DG.Tweening;
using UnityEngine;

public class GateTrap : MonoBehaviour
{
	public float angle;

	public float duration;

	private void Start()
	{
		base.transform.DORotate(new Vector3(base.transform.eulerAngles.x, base.transform.eulerAngles.y, angle), duration).SetLoops(-1, LoopType.Yoyo);
	}
}
