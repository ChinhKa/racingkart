using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Map Configs", menuName = "Data/Map/Map Configs")]
public class MapConfigs : ScriptableObject
{
    public List<Map> maps = new List<Map>();
}

[System.Serializable]
public class Map
{
    public string mapID;
    public string name;
    public Transform prefab;
}
