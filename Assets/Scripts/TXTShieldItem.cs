using TMPro;
using UnityEngine;

public class TXTShieldItem : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI txtShieldItem;

	private void OnEnable()
	{
		UpdateText();
		GameEvents.onUpdateShieldText.AddListener(UpdateText);
	}

	private void OnDisable()
	{
		GameEvents.onUpdateShieldText.RemoveListener(UpdateText);
	}

	private void UpdateText()
	{
		txtShieldItem.text = Prefs.SHIELD_ITEM.ToString();

		if(UIManager.ins != null)
			UIManager.ins.gamePlayWindow.ActiveButtonShieldItem();
	}
}
