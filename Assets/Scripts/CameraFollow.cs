﻿using System.Collections;
using UniRx;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private CompositeDisposable subscriptions = new CompositeDisposable();
    public Transform target;
    public ParticleSystem finishEffect;
    private bool isCameraSlow = true;
    public float distance = 5f; // Khoảng cách giữa camera và target
    public float heightOffset = 4f; // Độ chênh lệch độ cao so với target
    public float speedSlowFollow = 5f; // Tốc độ theo dõi

    private void OnEnable()
    {
        GameEvents.onCarBrake_Player.AddListener(() => {
            isCameraSlow = true;
        });

        GameEvents.onAccelerationCar_Player.AddListener(() =>
        {
            isCameraSlow = false;
        });

        StartCoroutine(Subscribe());
    }

    private void OnDisable()
    {
        subscriptions.Clear();
    }

    private IEnumerator Subscribe()
    {
        yield return new WaitUntil(() => GameEvents.instance != null);
        GameEvents.instance.finishGame.Subscribe(delegate (bool x)
        {
            if (x)
            {
                finishEffect.Play();
            }
        }).AddTo(subscriptions);
    }

    void LateUpdate()
    {
        if (target != null)
        {
            Vector3 targetPosition = target.position - target.parent.forward * distance + Vector3.up * heightOffset;

            transform.position = targetPosition;
            transform.LookAt(target.position + Vector3.up * heightOffset);
        }
    }
}

/*
[Header("Camera Controls")]
/// distance to the target
public float Distance = 5.5f;
/// height between target and camera
public float Height = 2.5f;
/// damping translation of the camera
public float DampingPosition = 0.02f;
/// damping steering lateral translation
public float DampingSteering = 0.5f;
/// steering translation impact to camera
public float SteeringOffset = 5f;
/// camera LookAt target offset
public float TargetLookUpOffset = 2f;
//protected Transform _target;
protected Vector3 currentLateralOffset = Vector3.zero;
protected Vector3 _moveVelocityReference;
protected Vector3 _targetPosition;
protected Vector3 _targetLateralTranslation;
void LateUpdate()
{
    *//* if (target != null)
     {
         Vector3 targetPosition = target.parent.position - target.parent.forward * distance + Vector3.up * heightOffset;

         transform.position = new Vector3(targetPosition.x, target.position.y + 6, targetPosition.z);
         transform.LookAt(target.position + Vector3.up * heightOffset);
     }*//*

    if (target == null)
    {
        return;
    }

    // the target position is computed depending on the vehicle's position and camera parameters
    _targetPosition = target.transform.position
        - (target.transform.forward * Distance)
        + (target.transform.up * Height);

    // we change camera position with a smooth damp
    transform.position = Vector3.SmoothDamp(transform.position, _targetPosition, ref _moveVelocityReference, DampingPosition);

    // we compute the new lateral translation value to smooth the vehicle's rotation
    _targetLateralTranslation = target.transform.right;

    // we save the current value in the camera GameObject in anticipation of the next Update
    currentLateralOffset = Vector3.Lerp(currentLateralOffset, _targetLateralTranslation, Time.deltaTime * DampingSteering);

    // we make the camera look at the vehicle, modified by the lateral and up offsets
    transform.LookAt(currentLateralOffset + target.transform.position + (target.up * TargetLookUpOffset));
}*/