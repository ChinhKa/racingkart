﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace PathCreation.Examples
{
	public class PathFollower : MonoBehaviour
	{
		public PathCreator pathCreator;

		public float distanceTravelled;

		public EndOfPathInstruction endOfPathInstruction;

		public float speedBase;
		private float speedBaseExpand;
		[HideInInspector]
		public float speed;
		private float speedMultiplier;

		[HideInInspector] public float accelerate;
		[HideInInspector] public float handling;

		[Header("RANK:")]
		public int rank;
		public GameObject rankParent;
		public TextMeshProUGUI txtRank;

		[Space]
		[Header("SIGNAL:")]
		public bool isAccelerating;

		private void Start()
		{
			pathCreator = GameManager.ins.pathCreator;

			if (pathCreator != null)
			{
				pathCreator.pathUpdated += OnPathChanged; 
			}
		}

		public void SetStartDistance(float startDistance) => StartCoroutine(AsyncSetStartDistance(startDistance));

		private IEnumerator AsyncSetStartDistance(float startDistance)
        {
			yield return new WaitUntil(() => pathCreator != null);
			distanceTravelled = startDistance;
			distanceTravelled = Mathf.Clamp(distanceTravelled, 0f, pathCreator.path.length);
			base.transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
			base.transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
        }

		private void UpdateRank()
		{
			rank = RankManager.ins.FindRank(this);
			txtRank.text = rank.ToString();
		}

		public void RespawnPos(Action<bool> success)
		{
			StartCoroutine(RespawnPosHandle(success));
		}

		public IEnumerator RespawnPosHandle(Action<bool> success = null)
		{
			yield return new WaitForSeconds(DataManager.ins.gameSettings.timeToRespawn);
			if (pathCreator != null)
			{
				isAccelerating = false;
				speed = 0;
				float value = pathCreator.path.GetClosestDistanceAlongPath(base.transform.position) - DataManager.ins.gameSettings.distanceToRespawn;
				distanceTravelled = Mathf.Clamp(value, 0f, pathCreator.path.length);
				if (success != null)
				{
					success(obj: true);
				}
				else
				{
					success(obj: false);
				}
			}
		}

		private void Update()
		{
			if (pathCreator != null)
			{
				if (isAccelerating)
				{
					Acceleration();
				}
				else
				{
					Deceleration();
				}
				if (speed > 0f)
				{
					distanceTravelled += (speed + speed * speedMultiplier) * Time.deltaTime;
				}
				base.transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
				base.transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
				speed = Mathf.Clamp(speed, 0f, speedBase + speedBaseExpand);
				UpdateRank();
			}
		}

		public void Deceleration()
		{
			speed -= 1f * accelerate;
		}

		public void Acceleration()
		{
			speed += 1f * accelerate;
		}

		public void SpeedExpand()
		{
			speedBaseExpand = speedBase * (DataManager.ins.gameSettings.nitroBoostPercent / 100f);
			speedMultiplier = DataManager.ins.gameSettings.nitroBoostPercent / 100f;
		}

		public void ResetSpeed()
		{
			speedBaseExpand = 0f;
			speedMultiplier = 0f;
			isAccelerating = false;
		}

		public void Nitro()
        {
			speed = speedBase + speedBaseExpand;
		}

		private void OnPathChanged()
		{
			distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(base.transform.position);
		}
	}
}
