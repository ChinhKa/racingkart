using EasyButtons;
using UnityEngine;
using UnityEngine.SceneManagement;
using PathCreation;
using PathCreation.Examples;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager ins;
    public Canvas canvas;
    public Camera cameraMain;
    public Camera cameraSecond;
    [HideInInspector] public PathCreator pathCreator;
    private Transform player;
    [SerializeField] private Transform groupMap;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        SpawnLevelMap();
        SpawnPlayerCar();
    }

    [Button]
    public void CameraFollowPlayer()
    {
        cameraMain.transform.GetComponent<CameraFollow>().target = player;
    }

    [Button]
    public void SpawnPlayerCar() => StartCoroutine(AsyncSpawnPlayerCar());

    private IEnumerator AsyncSpawnPlayerCar()
    {
        yield return new WaitUntil(() => DataManager.ins != null);
        if (player != null)
        {
            RankManager.ins.pathFollowers.Remove(player.GetComponent<PathFollower>());
            Destroy(player.gameObject);
        }
        var carConfigs = DataManager.ins.gameSettings.cars[int.Parse(Prefs.CAR_SELECTED_ID)];
        Transform car = Instantiate(carConfigs.prefab);
        car.GetChild(0).GetComponent<Movement>()._AI = false;
        car.GetChild(0).GetComponent<Movement>().SetData(carConfigs);
        car.GetComponent<PathFollower>().rankParent.SetActive(false);
        player = car.GetChild(0);
        player.tag = "Player";
        RankManager.ins.pathFollowers.Add(player.parent.GetComponent<PathFollower>());
        car.parent = groupMap;
    }

    [Button]
    public void SpawnLevelMap() => StartCoroutine(AsyncSpawnLevelMap());

    private IEnumerator AsyncSpawnLevelMap()
    {
        yield return new WaitUntil(() => DataManager.ins != null);
        var level = DataManager.ins.gameSettings.levelConfigs.levels[Prefs.LEVEL - 1];

        #region Spawn Map
        var mapInfo = DataManager.ins.gameSettings.mapConfigs.maps.Find(m => m.mapID == level.mapID);
        Transform map = Instantiate(mapInfo.prefab);
        pathCreator = PathCreator.FindObjectOfType<PathCreator>();
        map.parent = groupMap;
        #endregion

        #region Spawn Bot
        foreach (var b in level.bots)
        {
            var carConfigs = DataManager.ins.gameSettings.cars.Find(c => c.id == b.carID);
            if(carConfigs != null)
            {
                Transform car = Instantiate(carConfigs.prefab);
                car.GetComponent<PathFollower>().SetStartDistance(b.startDistance);
                car.GetChild(0).GetComponent<Movement>().SetData(carConfigs);
                car.GetChild(0).GetComponent<Movement>()._AI = true;
                RankManager.ins.pathFollowers.Add(car.GetComponent<PathFollower>());
                car.parent = groupMap;
            }
        }
        #endregion
    }

    [Button]
    public void LoadLevel(bool isNext)
    {
        if (isNext)
        {
            Prefs.LEVEL++;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    [Button]
    public void IncreaseNitroBoost()
    {
        Prefs.NITRO_ITEM++;
        GameEvents.onUpdateNitroText?.Invoke();
    }

    [Button]
    public void IncreaseShieldItem()
    {
        Prefs.SHIELD_ITEM++;
        GameEvents.onUpdateShieldText?.Invoke();
    }

    public void TurnOnCamera(bool isMainCam, bool isWorldCamera = false)
    {
        if (isMainCam)
        {
            groupMap.gameObject.SetActive(true);
            if (isWorldCamera)
            {
                canvas.renderMode = RenderMode.ScreenSpaceCamera;
                canvas.worldCamera = cameraMain;
            }
            else
            {
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            }
            cameraMain.gameObject.SetActive(true);
            cameraSecond.gameObject.SetActive(false);
        }
        else
        {
            groupMap.gameObject.SetActive(false);

            canvas.renderMode = RenderMode.ScreenSpaceCamera;

            canvas.worldCamera = cameraSecond;

            cameraMain.gameObject.SetActive(false);
            cameraSecond.gameObject.SetActive(true);
        }
    }

    [Button]
    public void StartGame()
    {
        GameEvents.instance.gameStarted.Value = true;
        CameraFollowPlayer();
    }

    [Button]
    public void ClearData()
    {
        PlayerPrefs.DeleteAll();
    }
}
